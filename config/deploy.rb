# config valid only for current version of Capistrano
lock '3.4.0'

set :application, 'american_grown'
set :repo_url, 'git@bitbucket.org:emagid/americangrowndiamonds.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp
set :user, "betaamericangrow"
# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/home/betaamericangrow/american"

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :pretty
set :format, :pretty

# Default value for :log_level is :debug
set :log_level, :debug

set :tmp_dir, "/home/betaamericangrow/tmp"

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
# set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'vendor/bundle', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 3

namespace :file do
    desc "Add Emagid lib"
    task :update_emaigd_lib do
        on roles(:app) do
            within current_path do 
                execute :rm, "-rf libs/Emagid"
                execute :git, "init"
                execute :git, "submodule add git@bitbucket.org:emagid/emagidphp.git libs/Emagid"
                within "libs/Emagid" do 
                    execute :git, "checkout dev"
                end
            end
        end
    end

    desc "Change index.php permission"
    task :permission_index do
        on roles(:app) do
            # execute :pwd
            # execute :echo, "#{current_path}"
            # execute :cd, "#{current_path}"
            # execute :chmod, '755 index.php'
        end
    end
end

namespace :config do
    desc "Create db config file symbol link" 
    task :db do
        on roles(:app) do
            execute :ln, "-s #{shared_path}/emagid.db.php #{release_path}/conf/"
        end
    end
    task :uploads do
        on roles(:app) do
            execute :rm, "-rf #{release_path}/content/uploads"
            execute :ln, "-s #{shared_path}/uploads #{release_path}/content/uploads"
        end
    end
end

namespace :deploy do
    after :finished, "file:update_emaigd_lib"
    after :finished, "config:db"
    after :finished, "config:uploads"
    after :finished, "file:permission_index"
end
