$(document).ready(function(){

    // =============================  Enhance Cam. ==================================
    window.video = document.getElementById("video");
    window.img = document.getElementById("filter");
    window.canvas2 = document.getElementById("pic");
    
    window.videoStream = null;
    window.ctx = canvas2.getContext("2d");
    window.export_format = 'image/jpeg';
    window.capture = { width: 804, height: 728 };
    // 1280, 720
    window.gen_cam_dims = {
      '720':{width: 1280, height: 720},
      '1080':{width: 1920, height: 1080}
    };
    window.cam_dims = gen_cam_dims['1080'];
    start();

    function start() {
      // return;
      if ((typeof window === 'undefined') || (typeof navigator === 'undefined')) console.log('This page needs a Web browser with the objects window.* and navigator.*!');
      else if (!(video && canvas2)) console.log('HTML context error!');
      else {
        console.log('Get user media…');
        
        var constrains = {
          audio: false,
          video: {
            mandatory: {
              maxWidth: cam_dims.width,
              maxHeight: cam_dims.height
            },
            optional: [{
              minWidth: cam_dims.width
            }, {
              minHeight: cam_dims.height
            }]
          }
        };
        console.log('constrains', constrains);
        navigator.getUserMedia = navigator.getUserMedia ||
          navigator.webkitGetUserMedia ||
          navigator.mozGetUserMedia;

        if (navigator.getUserMedia) {
          navigator.getUserMedia(constrains, gotStream, function(err) {
            console.log("The following error occurred: " + err.name);
          });
        } else {
          console.log("getUserMedia not supported");
        }
      }
    }

    function noStream() {
      console.log('Access to camera was denied!');
    }

    function stop() {
      if (videoStream) {
        if (videoStream.stop) videoStream.stop();
        else if (videoStream.msStop) videoStream.msStop();
        videoStream.onended = null;
        videoStream = null;
      }
      if (video) {
        video.onerror = null;
        video.pause();
        if (video.mozSrcObject)
          video.mozSrcObject = null;
        video.src = "";
      }
    }

    function gotStream(stream) {
      // if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        // navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
          // video.src = window.URL.createObjectURL(stream);
          window.ref_stream = stream;
          video.srcObject = stream;
          video.onloadedmetadata = function(e) {
            video.height = capture.height;
            console.log({vid:[video.videoWidth||null, video.videoHeight||null], capture:capture, elem: [$(video).width(), $(video).height()]});
            if (video.videoWidth && cam_dims.width == 1920 && video.videoWidth < cam_dims.width){
                track = stream.getTracks()[0];
                track.stop();
                stream.removeTrack(track);
                cam_dims = gen_cam_dims['720'];
                // canvas3.width = cam_dims.height / 1.5;
                // canvas3.height = cam_dims.height;
                start();
                return;
            }
            $(video).css({ marginRight: '-' + '' + (($(video).width() - $('.video-wrapper').width()) / 2) + 'px' });
            video.play();
          };
          // video.videoWidth  = img.width;
          // video.videoHeight = img.height;

          // video.play();
      //   });
      // }
    }






    // =============================  TAKE PHOTO  ==================================
    $(document).on('click', '#take_pic, .retake, .share', function(e){  
    e.preventDefault();      
        var self = this;
        $(self).css('transform', 'scale(.95)');
        setTimeout(function(){
            $(self).css('transform', 'scale(1)');
        }, 200);
    });

    function countDown() {
      $('.color_slide').fadeOut(500);
        var divs = $('.countdown');
        var timer
        var offset = 0 

        divs.each(function(){
            var self = this;
            timer = setTimeout(function(){
                $(self).css('font-size', '400px');
                $(self).fadeOut(1000);
            }, 1000 + offset);
            offset += 1000;
        });
    }

    function convertCanvasToImage(canvas) {
        var image = new Image();
        image.src = canvas.toDataURL("image/png");
        console.log(image)
        return image;
    }

    function showPictures() {
      setTimeout(function(){
        $('#pictures, .intro').fadeIn(1000);
        var photos = $('#pictures').children('.photo');
        $(photos[0]).fadeIn();

        $.post('/contact/save_img/',{image:$($(photos[0]).html()).attr('src')},function(data){
            $('#submit_form input[name=image]').val(data.image.id);
        });
      }, 1000);
    }


    // PHOTO CLICK
    $(document).on('click', '#take_pic',function(){
        $('#take_pic').css('pointer-events', 'none');
        $('#take_pic, .intro').fadeOut(500);
        $(this).fadeOut(500);
        var pics = []

        countDown();
        setTimeout(function(){
            var canvas = $('#pic');
            var context = canvas[canvas.length -1].getContext('2d');
            var video = document.getElementById('video');
            // var background = $('#filter')[0];

            context.drawImage(video, 0, 0, 1080, 810);
            // context.drawImage(background, 0, 0, 1080, 810);                   

            $($(canvas[canvas.length -1]).parents('.canvas_holder')[0]).html(convertCanvasToImage(canvas[canvas.length -1]))
            flash();

        }, 4000);

        setTimeout(function(){
            $('.step_one').fadeOut(1000);
            $('.submit, .retake').delay(1000).fadeIn();
            $('.print').delay(1000).fadeIn();
            $(this).css('pointer-events', 'all');
            showPictures();
        }, 4000);
    });

    function flash(){
      console.log('hi')
      $('.flash').fadeIn(300);
      setTimeout(function(){
        $('.flash').fadeOut(300);
      }, 300);
    }



    // =============================  SHARING PHOTO  ==================================
    $(document).on('click', '.retake', function(){
      setTimeout(function(){
        window.location = '/';
      }, 500);
    });

    $(document).on('click', '.share', function(){
        // $.post('/contact/save_img/',{image:$('#pictures > img:last-child').attr('src')},function(data){
        //     $('#submit_form input[name=image]').val(data.image.id);
        // });
        $('.step_two').fadeOut(500);
        setTimeout(function(){
          $('.step_three').fadeIn(500);
        }, 500);

        $('.fa-close').fadeIn();
     });

    // $(document).on('click', '.input', function(){
    //   $('.step_three').css('margin-top', '500px');
    // });

    $(document).on('click', '.fa-close',function(){
      $('.step_three').css('margin-top', '100px');
        $('.jQKeyboardContainer').slideUp();
        $('.step_three').slideUp(500);
        setTimeout(function(){
          $('.step_two').slideDown(500);
        }, 500);
        $('.fa-close').fadeOut();
    });


     $(document).on('click', '#share_btn', function(){
          $('.fa-close').fadeOut();
          $('.jQKeyboardContainer').fadeOut();

          $('#share_alert').slideDown();
          $('#share_alert').css('display', 'flex');
          $('#submit_form').submit();
          $.post('/contact', $('#submit_form').serialize(), function (response) {
              if(response.status){
                  $('#submit_form')[0].reset();
                  setTimeout(function(){
                    window.location.href = '/';
                  }, 5000);
              }else {
                setTimeout(function(){
                    window.location.href = '/';
                  }, 5000);
              }
          });

          setTimeout(function(){
            window.location.href = '/';
          }, 10000);
     });

     $('#submit_form').submit(function(e){
      e.preventDefault();
     });





    // =============================  FILTERS  ==================================
      $('.filter_btn').click(function(){
        $('.filters_holder').css('width', '900px');
        $('.off_click').fadeIn(300);
        setTimeout(function(){
          $('.filters_holder div').fadeIn(300);
        }, 500);
        $('.filters_holder div');
      });

      $('.off_click, .fa-close').click(function(){
        $('.filters_holder div').fadeOut();
        $('.filters_holder').css('width', '0');
        $('.off_click').fadeOut(300);
        setTimeout(function(){
          $('.filters_holder .fa-close').show();
        }, 1000)
      });

      $('.filters_holder h2').click(function(){
        $('.filters_holder h2').removeClass('filter_active');
        $(this).addClass('filter_active');
      });

        $('#add_email').click(function(){
            $('.first_email').parent().append("<input required class='input jQKeyboard' name='email[]' pattern='[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$' type='text' placeholder='Email' title='Please enter a valid email address.'>");
            $('.first_email').parent().find('input').last().initKeypad({'keyboardLayout': keyboard});
        });

        $('#add_phone').click(function(){
            $('.first_phone').parent().append("<input required class='input jQKeyboard' name='phone[]' placeholder='Phone Number (eg: +19874563210)'>");
            $('.first_phone').parent().find('input').last().initKeypad({'keyboardLayout': keyboard});
        });

        $('#video').fadeIn(1000);

        setTimeout(function(){
            $('.pic_text').fadeIn(2000);
            // $('#take_pic').fadeIn(2000);
            $('#snap_gif').fadeIn(2000);
        }, 1000);

        setTimeout(function(){
            $('#video').fadeIn(1000);
        }, 2000);



       // Gif Next click
       function picAction( pic ) {
        $(pic).fadeIn('fast');
        $(pic).delay(1300).fadeOut(400);
      }

       function gif( pics ) {
        var offset = 0

        for ( i=0; i<2; i++ ) {
          pics.each(function(){
            var timer
            var self = this;
            timer = setTimeout(function(){
                picAction(self)
            }, 0 + offset);

            offset += 1500;
          });
        }
      }


       $('.print').click(function(){
            saveDrawing();  
            var source = $($('#pictures img:last-child')[1]).attr('src');
            VoucherPrint(source);
       });


       function convertCanvasToImage(canvas) {
          var image = new Image();
          image.src = canvas.toDataURL("image/png");
          console.log(image)
          return image;
      }
 
});