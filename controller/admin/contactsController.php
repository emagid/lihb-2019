<?php

class contactsController extends adminController {
	
	function __construct(){
		parent::__construct("Contact", "contacts");
	}
	
	function index(Array $params = []){
		$this->_viewData->hasCreateBtn = true;		

	

		parent::index($params);
	}

	function update(Array $arr = []){
		$cat = new $this->_model(isset($arr['id'])?$arr['id']:null);
		$this->_viewData->credits = \Model\Credit::getList(['where'=>" active = 1"]);
		
		parent::update($arr);
	}
  
}