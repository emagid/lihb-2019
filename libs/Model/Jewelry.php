<?php
namespace Model;

class Jewelry extends \Emagid\Core\Model{

    public static $tablename = 'jewelry';
    public static $fields = [
        'name',
        'description',
        'price',
        'currency_code',
        'quantity',
        'tags',
        'materials',
        'images',
        'variation',
        'meta_title',
        'meta_keywords',
        'meta_description',
        'metal_colors',
        'metals',
        'total_14kt_price',
        'total_18kt_price',
        'total_plat_price',
        'slug',
        'semi_mount',
        'stone_breakdown',
        'stone_count',
        'weight',
        'diamond_cwwt',
        'diamond_color',
        'diamond_quality',
        '_14kt',
        '_18kt',
        'platinum',
        'setting',
        'polish',
        'melee',
        'ship_from_cast',
        'ship_to_cust',
        'featured',
        'discount'
    ];

    static $relationships = [
        [
            'name' => 'product_category',
            'class_name' => '\Model\Product_Category',
            'local' => 'id',
            'remote' => 'product_map_id',
            'remote_related' => 'category_id',
            'relationship_type' => 'many'
        ],
    ];

    //hardcoded until further notice
    public static $additionalCharge = [
        'style'=>[
            '3_prongs'=>0,'4_prongs'=>50
        ],
        'back'=>[
            'push'=>0,'screw'=>50
        ],
        'length'=>[
            '16_inch'=>0,'18_inch'=>50,'20_inch'=>100
        ]];
    public static $searchFields = ['name'];

    public static function searchMetaData()
    {
        $data = [];
        $objects = self::getList();
        foreach($objects as $object){
            $fieldData = [];
            foreach(self::$searchFields as $field){
                $fieldData[] = $object->$field;
            }

            $data[] = ['fields' => $fieldData, 'object' => $object];
        }

        return $data;
    }

    public function generateSearchResult()
    {
        return ['name' => $this->name, 'image' => $this->featuredImage(), 'link' => "/jewelry/{$this->slug}"];
    }

    public static function defaultMetalColors(){
        return '{"14kt_gold":{"White":0,"Yellow":0,"Rose":0},"18kt_gold":{"White":0,"Yellow":0,"Rose":0},"Platinum":{"White":0,"Yellow":0,"Rose":0}}';
    }

    public function getPriceDiff($selectMetal)
    {
        $price = 0;
        switch(strtoupper($selectMetal)){
            case '14KT GOLD':
                return 0;
            case '18KT GOLD':
                $price = floatval($this->total_18kt_price) - floatval($this->total_14kt_price);
                break;
            case 'PLATINUM':
                $price = floatval($this->total_plat_price) - floatval($this->total_14kt_price);
                break;
        }

        if($this->featured && $this->discount){
            $price = $price * (100 - floatval($this->discount));
        }

        return $price;
    }

    public function featuredImage(){
        $productImage = new Product_Image();
        $productImage = $productImage->getProductImage($this->id, 'jewelry');
        if(!empty($productImage)){
            return UPLOAD_URL."products/".$productImage[0]->image;
        }

        return FRONT_ASSETS."img/ring5.png";
    }

    public function getImages(){
        $jewelryImage = new Product_Image();
        $jewelryImage = $jewelryImage->getProductImage($this->id, 'Jewelry');
        $imageArray = [];
        if($jewelryImage){
            // get rid of featured image
            foreach($jewelryImage as $image){
                $imageArray[] = UPLOAD_URL."products/".$image->image;
            }
        }
        return $imageArray;
    }

    public function getUrl()
    {
        return "/jewelry/{$this->id}";
    }


    public function getMap()
    {
        $map = new Product_Map();
        return $map->getMap($this->id, 'Jewelry');
    }


    public static function getMetals(){
        return ['14kt gold', '18kt gold', 'Platinum'];
    }

    public static function getColors(){
        return ['White','Yellow','Rose'];
    }

    public function getPrice($selectMetal = '14KT GOLD', $stone = null)
    {
        $price = $this->total_14kt_price;
        switch(strtoupper($selectMetal)){
            case '14KT GOLD':
                $price = $this->total_14kt_price;
                break;
            case '18KT GOLD':
                $price = $this->total_18kt_price;
                break;
            case 'PLATINUM':
                $price = $this->total_plat_price;
        }

        $price = floatval($price);

        if($this->featured && $this->discount){
            $price = $price * (100 - floatval($this->discount)) / 100;
            $price = floatval($price);
        }

        return $price;
    }

    public function getOriginalPrice($selectMetal = '14KT GOLD')
    {
        $price = $this->total_14kt_price;
        switch(strtoupper($selectMetal)){
            case '14KT GOLD':
                $price = $this->total_14kt_price;
                break;
            case '18KT GOLD':
                $price = $this->total_18kt_price;
                break;
            case 'PLATINUM':
                $price = $this->total_plat_price;
        }

        return $price;
    }

    public static function randomizer($limit = 10){
        $items = self::getList();
        shuffle($items);
        return array_slice($items,0,$limit);
    }

    public function getCategories(){
        if($map = $this->getMap()) {
            $prod_cat = Product_Category::getList(['where' => "product_map_id = {$map->id}"]);
            $arr = [];
            foreach ($prod_cat as $pc) {
                $arr[] = Category::getItem($pc->category_id);
            }
            return $arr;
        }
        return '';
    }
}