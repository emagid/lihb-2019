<?php

namespace Model;

class Snapshot_Contact extends \Emagid\Core\Model
{

    static $tablename = "snapshot_contact";

    public static $fields = [
        'image',
        'contact_id',
        'in_slider'
    ];

    public static function slider(){
        $images = [];
        foreach( self::getList(['where'=>"active = 1"]) as $image){
            $images[] = UPLOAD_URL.'Snapshots'.DS.$image->image;
        }
        return json_encode($images);
    }

    /**
     * checks that the image exists in the path
     *
     * @param type $size : size of an image to check if that size exists
     * @return boolean: true if file exists, false otherwise
     */
    public function exists_image()
    {
        if ($this->image != "" && file_exists(UPLOAD_PATH . 'Snapshots' . DS . $this->image)) {
            return true;
        }
        return false;
    }

    /**
     * builds url link to the image in the specified path.
     *
     * @param type $size : optional size to get the image with that specific size
     * @return type: url to image
     */
    public function get_image_url()
    {
        return UPLOAD_URL . 'Snapshots/' . $this->image;
    }
}

