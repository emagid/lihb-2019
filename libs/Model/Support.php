<?php

namespace Model;

class Support extends \Emagid\Core\Model {
    static $tablename = "public.hccsupport";

    public static $fields  =  [
        'support_list',
        'phone' => ['required'=>true],
    ];

}