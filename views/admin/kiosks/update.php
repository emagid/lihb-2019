<form class="form" action="<?= $this->emagid->uri ?>" method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" value="<?php echo $model->kiosk->id ?>"/>

    <div role="tabpanel">
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active"><a href="#general-tab" aria-controls="general" role="tab" data-toggle="tab">General</a></li>
            <li role="presentation"><a href="#videos-tab" aria-controls="content" role="tab" data-toggle="tab">Videos</a></li>
            <li role="presentation"><a href="#survey-tab" aria-controls="content" role="tab" data-toggle="tab">Survey</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="general-tab">
                <div class="row">
                    <div class="col-md-16">
                        <div class="box">
                            <h4>General</h4>
                            
                            <div class="form-group">
                                <label>Name</label>
                                <?php echo $model->form->editorFor("name"); ?>
                            </div>
                            <div class="form-group">
                                <label>Location/Description</label>
                                <?php echo $model->form->textAreaFor("description"); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="videos-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Video Page Content</h4>
                            <div class="form-group">
                                <label>Videos</label>
                                <br/>
                                <select multiple data-id='videos' id='videos_select'>
                                    <? foreach(\Model\Video::getList(['']) as $video) {?>
                                        <? $selected = in_array($video->id,$model->content->videos)?'selected':'' ?>
                                        <? $selected = '' ?>
                                        <option value="<?=$video->id?>" <?=$selected?>><?=$video->title?></option>
                                    <? } ?>
                                </select>
                                <input type='hidden' id='videos' name='content[videos]' value='<?=json_encode($model->content->videos)?>' />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="survey-tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>Survey Page Content</h4>
                            <div class="form-group">
                                <label>Survey</label>
                                <br/>
                                <select multiple data-id='surveys' id='surveys_select'>
                                    <? foreach(\Model\Survey::getList() as $survey) {?>
                                        <? $selected = in_array($survey->id,$model->content->surveys)?'selected':'' ?>
                                        <? $selected = '' ?>
                                        <option value="<?=$survey->id?>" <?=$selected?>><?=$survey->title?></option>
                                    <? } ?>
                                </select>
                                <input type='hidden' id='surveys' name='content[surveys]' value='<?=json_encode($model->content->surveys)?>' />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="4_tab">
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <h4>iFrame Content</h4>
                            <div class="form-group">
                                <label>URL</label>
                                <br/>
                                <input type='text' name='content[url]' value='<?=$model->content->url?>'/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-24">
            <button type="submit" class="btn btn-success btn-lg">Save</button>
        </div>
    </div>
</form>


<?= footer(); ?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.jquery.min.js"></script>
<script type="text/javascript" src="<?=ADMIN_JS?>chosen.order.jquery.min.js"></script>
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/chosen/1.8.5/chosen.min.css" />

<script type='text/javascript'>
    $(document).ready(function () {

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    var img = $("<img />");
                    img.attr('src', e.target.result);
                    img.attr('alt', 'Uploaded Image');
                    img.attr("width", '100');
                    img.attr('height', '100');
                    $("#preview-container").html(img);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        
        $("select[multiple]").chosen({width:'100%'});
        $('form').submit(function(){
            $("select[multiple]").each(function(){
                var id= $(this).data('id');
                $('input#'+id).val('['+$(this).getSelectionOrder()+']');
            });
        });
        <? if(count($model->content->videos)) {?>
            ChosenOrder.setSelectionOrder($('#videos_select'),<?=json_encode($model->content->videos)?>,true);
        <? } ?>
        <? if(count($model->content->surveys)) {?>
            ChosenOrder.setSelectionOrder($('#surveys_select'),<?=json_encode($model->content->surveys)?>,true);
        <? } ?>

        

        $("[name=template]").change(function(){
            console.log('sdsds');
            var tab = '#'+this.value+'_tab';
            $('#content_tab_heading').attr('href',tab);
            $('.tab-pane').not('#general-tab').find(':input').prop('disabled',true);
            $(tab).find(':input').prop('disabled',false);
        }).change();

        $("input.image").change(function () {
            readURL(this);
            $('#previewupload').show();
        });

        $("input[name='title']").on('keyup', function (e) {
            var val = $(this).val();
            val = val.replace(/[^\w-]/g, '-');
            val = val.replace(/[-]+/g, '-');
            $("input[name='slug']").val(val.toLowerCase());
        });
        var val = <?=isset($val)?$val:0?>;
        $('#add_desc').on('click', function(){
            var content = $(
                '<div class="form-group"><input type="text" name="content_title[]" placeholder="Content Header"><textarea id="editor_'+val+'" class="ckeditor" name="content_description[]"></textarea><label>Order</label><input type="number" name="content_order[]" value="'+val+'" placeholder="Enter display order"><br><a href="#" class="delete_content">Delete</a></div>'
            );
            $('#content_box').append(content);
            triggerCK();
        });

        function triggerCK(){
            var editor = 'editor_'+val;
            CKEDITOR.replace(editor);
            val++;
        }

        //Delete function for removing container <div>. Revise when adding or removing elements
        $(document).on('click','.delete_content', function(){
            $(this).parent().remove();
        });
    });

</script>