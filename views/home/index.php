<div class='content'>
  <section class='main photobooth_page'>
    <!-- Step 1 -->
    <div class='step_one'>
      <img id='filter' src="<?=FRONT_ASSETS?>img/banner.png">
        <video id="video"  autoplay></video>
            <device type="media" onchange="update(this.data)"></device>
            <script>
                // function update(stream) {
                //     document.querySelector('video').src = stream.url;
                // }

                // var video = document.getElementById('video');

                // if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                //     navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                //         try {
                //           video.src = window.URL.createObjectURL(stream);
                //         } catch(error) {
                //           video.srcObject = stream;
                //         }
                //         video.play();
                //     });
                // }

                // $(document).on('click', function(){
                //   var video = document.getElementById('video');

                // if(navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
                //     navigator.mediaDevices.getUserMedia({ video: true }).then(function(stream) {
                //         try {
                //           video.src = window.URL.createObjectURL(stream);
                //         } catch(error) {
                //           video.srcObject = stream;
                //         }
                //         video.play();
                //     });
                // }
                // })
                
            </script>

      <div id='take_pic' class='button'>
        <img src="<?= FRONT_ASSETS ?>img/cam.png">
        <p class='lrg'>Take a selfie</p>
        <div class='pulse'></div>
      </div>

      <div class='intro'>
        <p>Take a Selfie!</p>
      </div>

      <div class='countdown'>3</div>
      <div class='countdown'>2</div>
      <div class='countdown'>1</div>
    </div>

    <div class='flash'></div>


    <!-- Step 2 -->
    <div id='pictures' class='step_two'>
      <div class='canvas_holder photo'><canvas id='pic' class='canvas' width='1080' height='810'></canvas></div>
      <div class='btns'>
        <div class='retake button white_button'>
          <img src="<?= FRONT_ASSETS ?>img/share.png">
          <p class='lrg'>Retake</p>
        </div>
        <div class='share button'>
          <img src="<?= FRONT_ASSETS ?>img/share.png">
          <p class='lrg'>Share</p>
          <div class='pulse'></div>
        </div>
      </div>

      <div class='intro'>
        <p>Click above to share!</p>
      </div>
    </div>


    <!-- Step 3 -->
    <div class='step_three'>
      <div class='card full'>
        <i class="fa fa-close sharex" style="font-size:36px"></i>
        <form id='submit_form'>
          <input type='hidden' name='form' value="1">
            <input type="hidden" name="image" class="image_encoded">
            <span>
                <input class='input jQKeyboard first_email' name='email[]' pattern="[A-z0-9._%+-]+@[A-z0-9.-]+\.[A-z]{2,3}$" type='text' placeholder='Email' title='Please enter a valid email address.'>
            </span>
            <p id='add_email'>Add an email +</p>

            <span>
                <input class='input jQKeyboard first_phone' name="phone[]" placeholder="Phone Number (eg: +19874563210)">
            </span>
            <p id='add_phone'>Add a phone number +</p>
        </form>
      </div>

      <div id='share_btn' class='button'>
        <p class='lrg'>Send Picture</p>
        <div class='pulse'></div>
      </div>
    </div>

  </section>
        <!-- Alerts -->
  <section id='share_alert'>
    <h2>Thank You!</h2>
    <p>An email is on it's way!</p>
  </section>

  <script type="text/javascript" src='https://cdnjs.cloudflare.com/ajax/libs/fabric.js/2.4.6/fabric.min.js'></script>
  <script src="<?=auto_version(FRONT_JS."photobooth.js")?>"></script>
</div>


  


